/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rhalatiu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 19:27:20 by rhalatiu          #+#    #+#             */
/*   Updated: 2017/12/06 21:18:42 by rhalatiu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(char const *str)
{
	int count;
	int nb;
	int sign;

	count = 0;
	nb = 0;
	sign = 1;
	while ((*str == ' ') || (*str >= 9 && *str <= 13))
		str++;
	if (*str == '+' || *str == '-')
	{
		sign = (*str == '-' ? -1 : 1);
		str++;
	}
	while (ft_isdigit(*str))
	{
		count++;
		if (count == 20 && sign < 0)
			return (0);
		if (count == 20 && sign > 0)
			return (-1);
		nb = nb * 10 + *str - '0';
		str++;
	}
	return (nb * sign);
}
